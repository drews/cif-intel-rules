# Rule Naming

Make sure rules end in either `.yml` or `.yml.j2`.  `.yml.j2` files will be
processed as Jinja2 templates, with secrets below passed in to the environment

# Secrets

Don't put secrets in here, instead, throw them in vault.

Basic Usage:

```
export VAULT_ADDR=https://vault-systems.oit.duke.edu
vault login -method=ldap username=NETID
# Wait for Duo push to your phone
# Read existing values with
vault kv get itso/cif/smrt-rule-credentials
# Add a new entry with
vault kv patch itso/cif/smrt-rule-credentials foo_token=bar
```

Notes that `foo_token` will become `FOO_TOKEN` in the smrt environment
